
         
        document.write ( "hello world from javascript document writer by calling externally")  // display the content in HTML document itself.
        console.log("Hello World for Java script by calling externally") //display Hello World in the console
        
        // Comments Style:1  Using " <!--"
        
        <!-- FYI..  this "<!--"  considers as comments in java script also. console.log("tets for html comments") 
            
        //-->  FYI....the closing sequence ""-->" will not be considered in java script.. hence used //-->
        
        // Java script Comment Style:2  Using "/*...*/" 

        /*  FYI.. Its considers the below text as comments,.. which means its not displayed in the console..
            console.log ( " test for block comments")
        */ 

        // Java scrpt comments style:3 using "//"
       // FYI .. This is for single line comments

 